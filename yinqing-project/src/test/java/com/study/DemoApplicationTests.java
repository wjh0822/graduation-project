package com.study;

import com.alibaba.fastjson.JSONObject;
import com.study.common.utils.DateTimeUtil;
import com.study.impl.CumulativeTrendServiceImpl;
import com.study.impl.DomesticInformationServiceImpl;
import com.study.impl.NewTrendServiceImpl;
import com.study.request.AreaDataRequest;
import com.study.request.CumulativeTrendRequest;
import com.study.request.DomesticInformationRequest;
import com.study.request.NewTrendRequest;
import com.study.response.DomesticInformationResponse;
import com.study.service.AreaDataService;
import com.study.service.BasicDataAddService;
import com.study.service.BasicDataService;
import com.study.service.NewTrendService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class DemoApplicationTests {

    @Autowired
    private CumulativeTrendServiceImpl cumulativeTrendService;

    @Autowired
    private NewTrendServiceImpl newTrendServiceImpl;

    @Autowired
    private DomesticInformationServiceImpl domesticInformationService;

    @Autowired
    private BasicDataService basicDataService;

    @Autowired
    private BasicDataAddService basicDataAddService;

    @Autowired
    private NewTrendService newTrendService;

    @Autowired
    private AreaDataService areaDataService;


    @Test
    void contextLoads() {
        cumulativeTrendService.getCumulativeTrend();
    }

    @Test
    void queryCumulativeTrend() {
        System.out.println(JSONObject.toJSONString(cumulativeTrendService.queryCumulativeTrend(new CumulativeTrendRequest())));
    }

    @Test
    void addCumulativeTrend() {
        cumulativeTrendService.addCumulativeTrend();
    }

    @Test
    void insert() {
        newTrendServiceImpl.getNewTrend();
    }

    @Test
    void insertByForeach() {
        domesticInformationService.getDomesticInformation();
    }

    @Test
    void queryDomesticInformation() {
        DomesticInformationRequest domesticInformationRequest = new DomesticInformationRequest();
        domesticInformationRequest.setEventTime("2021-02-21");
        DomesticInformationResponse domesticInformationResponse = domesticInformationService.queryDomesticInformation(domesticInformationRequest);
        System.out.println(JSONObject.toJSONString(domesticInformationResponse.getList()));

    }

    @Test
    void insertByForeachByBasicData() {
        basicDataService.getBasicData();
    }

    @Test
    void insertByForeachByBasicDataAdd() {
        basicDataAddService.getBasicDataAdd();
    }

    @Test
    void queryBasicDataLast() {
        System.out.println(basicDataService.queryBasicDataLast().getList().get(0).getLocalTime());
    }

    @Test
    void queryNewTrend() {
        System.out.println(newTrendService.queryNewTrend(new NewTrendRequest()).getList().get(0).getDate());
    }

    @Test
    void addNewTrend() {
        System.out.println(newTrendService.addNewTrend());
    }

    @Test
    void getAreaData() {
        areaDataService.getAreaData();
    }

    @Test
    void queryAreaData() {
        AreaDataRequest areaDataRequest = new AreaDataRequest();
        areaDataRequest.setArea("香港");
//        areaDataRequest.setDate(DateTimeUtil.getNow("yyyy-MM-dd"));
        System.out.println(JSONObject.toJSONString(areaDataService.queryAreaData(areaDataRequest).getList()));

    }
}
