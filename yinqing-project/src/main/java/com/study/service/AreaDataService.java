package com.study.service;

import com.study.request.AreaDataRequest;
import com.study.response.AreaDataResponse;
import com.study.response.BasicDataResponse;
import org.springframework.stereotype.Service;

/**
 * @Author: liyuankun
 * @Date: 2021/2/9 16:53
 * @Description: 区域数据
 **/
@Service
public interface AreaDataService {

    public AreaDataResponse queryAreaData(AreaDataRequest request);

    public AreaDataResponse getAreaData();


}
