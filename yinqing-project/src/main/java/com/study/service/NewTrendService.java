package com.study.service;

import com.study.request.NewTrendRequest;
import com.study.response.NewTrendResponse;
import org.springframework.stereotype.Service;

/**
 * @Author: liyuankun
 * @Date: 2021/1/25 19:49
 * @Description: 全国疫情新增趋势
 **/

@Service
public interface NewTrendService {

    public void getNewTrend();

    public NewTrendResponse queryNewTrend(NewTrendRequest request);

    public NewTrendResponse addNewTrend();
}
