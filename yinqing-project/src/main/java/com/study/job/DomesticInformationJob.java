package com.study.job;

import com.study.impl.DomesticInformationServiceImpl;
import com.study.service.DomesticInformationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @Author: liyuankun
 * @Date: 2021/1/26 13:24
 * @Description: 国内资讯定时任务
 **/
@Component
public class DomesticInformationJob {

    private static final Logger logger = LoggerFactory.getLogger(DomesticInformationJob.class);

    @Autowired
    private DomesticInformationService domesticInformationService;

    @Scheduled(cron = "* */30 * * * ?")
    private void process(){

        try {
            domesticInformationService.getDomesticInformation();
        } catch (Exception e) {
            logger.error("<<<<<< DomesticInformationJob  process  error cause", e);
        }


    }

}
