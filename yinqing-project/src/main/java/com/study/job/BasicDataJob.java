package com.study.job;

import com.study.impl.BasicDataServiceImpl;
import com.study.service.BasicDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @Author: liyuankun
 * @Date: 2021/1/28 16:16
 * @Description:  基础数据定时任务
 **/
@Component
public class BasicDataJob {

    private static final Logger logger = LoggerFactory.getLogger(BasicDataJob.class);

    @Autowired
    private BasicDataService basicDataService;

    @Scheduled(cron = "0 0/30 10-22 * * ?")
    private void process(){

        try {
            basicDataService.getBasicData();
        }catch (Exception e) {
            logger.error("<<<<<< BasicDataJob  process  error cause", e);
        }


    }

}
