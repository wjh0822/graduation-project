package com.study.job;

import com.study.impl.CumulativeTrendServiceImpl;
import com.study.impl.NewTrendServiceImpl;
import com.study.service.CumulativeTrendService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @Author: liyuankun
 * @Date: 2021/2/9 14:07
 * @Description:  累计趋势
 **/
@Component
public class CumulativeTrendJob {

    private static final Logger logger = LoggerFactory.getLogger(CumulativeTrendJob.class);

    @Autowired
    private CumulativeTrendService cumulativeTrendService;

    @Scheduled(cron = "0 0 10 * * ?")
    private void process(){

        try {
            cumulativeTrendService.addCumulativeTrend();
        } catch (Exception e) {
            logger.error("<<<<<< CumulativeTrendJob  process  error cause", e);
        }


    }


}
