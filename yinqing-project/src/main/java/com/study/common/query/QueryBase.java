package com.study.common.query;

import java.io.Serializable;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QueryBase implements Serializable{

  private static final Logger logger = LoggerFactory.getLogger(QueryBase.class);

  private static final long serialVersionUID = 7603300820027561064L;
  private static final Integer DEFAULT_PAGE_SIZE = new Integer(20);

  private static final Integer DEFAULT_FRIAT_PAGE = new Integer(1);

  private static final Integer DEFAULT_TOTLE_ITEM = new Integer(0);
  private Integer totalItem;
  private Integer pageSize;
  private Integer currentPage;
  private Integer pageNo;

  protected final Integer getDefaultPageSize()
  {
    return DEFAULT_PAGE_SIZE;
  }

  public boolean isFirstPage()
  {
    return getCurrentPage().intValue() == 1;
  }

  public int getPreviousPage()
  {
    int back = getCurrentPage().intValue() - 1;

    if (back <= 0) {
      back = 1;
    }
    return back;
  }

  public boolean isLastPage()
  {
    return getTotalPage() == getCurrentPage().intValue();
  }

  public int getNextPage()
  {
    int back = getCurrentPage().intValue() + 1;

    if (back > getTotalPage()) {
      back = getTotalPage();
    }

    return back;
  }

  public Integer getCurrentPage()
  {
    if (this.currentPage == null) {
      return DEFAULT_FRIAT_PAGE;
    }

    return this.currentPage;
  }

  public void setCurrentPage(Integer cPage)
  {
    if ((cPage == null) || (cPage.intValue() <= 0)) {
      this.currentPage = DEFAULT_FRIAT_PAGE;
    }
    else {
      this.currentPage = cPage;
    }
  }

  public void setCurrentPageString(String s)
  {
    if (StringUtils.isBlank(s)) {
      return;
    }
    try
    {
      setCurrentPage(Integer.valueOf(Integer.parseInt(s)));
    } catch (NumberFormatException ignore) {
      setCurrentPage(DEFAULT_FRIAT_PAGE);
    }
  }

  public Integer getPageSize()
  {
    if (this.pageSize == null) {
      return getDefaultPageSize();
    }

    return this.pageSize;
  }

  public boolean hasSetPageSize()
  {
    return this.pageSize != null;
  }

  public void setPageSize(Integer pSize)
  {
    if (pSize == null) {
      throw new IllegalArgumentException("PageSize can't be null.");
    }

    if (pSize.intValue() <= 0) {
      throw new IllegalArgumentException("PageSize must great than zero.");
    }

    this.pageSize = pSize;
  }

  public void setPageSizeString(String pageSizeString)
  {
    if (StringUtils.isBlank(pageSizeString)) {
      return;
    }
    try
    {
      Integer integer = new Integer(pageSizeString);
      setPageSize(integer);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      logger.error("setPageSizeString >>>>>>>>>>>>>>>>>>>>>>>>> " , localNumberFormatException);
    }
  }

  public Integer getTotalItem()
  {
    if (this.totalItem == null) {
      return DEFAULT_TOTLE_ITEM;
    }
    return this.totalItem;
  }

  public void setTotalItem(Integer tItem)
  {
    if (tItem == null) {
      tItem = new Integer(0);
    }
    this.totalItem = tItem;
    int current = getCurrentPage().intValue();
    int lastPage = getTotalPage();
    if (current > lastPage) {
      setCurrentPage(new Integer(lastPage));
    }
  }

  public int getTotalPage()
  {
    int pgSize = getPageSize().intValue();
    int total = getTotalItem().intValue();
    int result = total / pgSize;
    if (total % pgSize != 0) {
      result++;
    }
    return result;
  }

  public int getPageFristItem()
  {
    int cPage = getCurrentPage().intValue();
    if (cPage == 1) {
      return 1;
    }
    cPage--;
    int pgSize = getPageSize().intValue();

    return pgSize * cPage + 1;
  }

  public int getMysqlPageFristItem()
  {
    return getPageFristItem() - 1;
  }

  public int getPageLastItem()
  {
    int assumeLast = getExpectPageLastItem();
    int totalItem = getTotalItem().intValue();

    if (assumeLast > totalItem) {
      return totalItem;
    }
    return assumeLast;
  }

  public int getExpectPageLastItem()
  {
    int cPage = getCurrentPage().intValue();
    int pgSize = getPageSize().intValue();
    return pgSize * cPage;
  }

	public Integer getPageNo() {
		return pageNo;
	}
	
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}
  
  
}