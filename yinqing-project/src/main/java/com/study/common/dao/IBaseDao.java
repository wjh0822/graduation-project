package com.study.common.dao;

import com.study.common.domain.IBaseDomain;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * mapper基类
 * </p>
 * 
 * @author zhangx
 * @version $Id: IBaseDao.java 2020年2月21日 下午3:29:49 zhangx $
 */
public interface IBaseDao<T extends IBaseDomain> {
	/**
	 * 根据条件返回列表
	 * @Title: getAll
	 * @Description: TODO 根据条件返回列表
	 * @param map
	 * @return List
	 */
	public List<T> getAll(Map<String, Object> map);

	/**
	 * 根据条件返回列表总记录数
	 * @Title: getAllCount
	 * @Description: TODO 根据条件返回列表总记录数
	 * @param map
	 * @return int
	 */
	public Integer getAllCount(Map<String, Object> map);

//	public void add(T object);
//
//	public void update(T object);

}
