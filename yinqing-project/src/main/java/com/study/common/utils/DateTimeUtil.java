package com.study.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * Created by 
 */
public class DateTimeUtil {
	private static final Logger log = LoggerFactory.getLogger(DateTimeUtil.class);
	
	/**
	 * 预设不同的时间格式
	 */
	/**
	 * 精确到年月日（英文） eg:20191111
	 */
	public static String FORMAT_LONG = "yyyyMMdd"	;
	/**
	 * 精确到年月日（英文） eg:2019-11-11
	 */
	public static String FORMAT_LONOGRAM = "yyyy-MM-dd"	;
	/**
	 * 精确到时分秒的完整时间（英文） eg:2010-11-11 12:12:12
	 */
	public static String FORMAT_FULL = "yyyy-MM-dd HH:mm:ss";
	/**
	 * 精确到毫秒完整时间（英文） eg:2019-11-11 12:12:12.55
	 */
	public static String FORMAT_FULL_MILL = "yyyy-MM-dd HH:mm:ss.SSS";
	/**
	 * 精确到年月日（中文）eg:2019年11月11日
	 */
	public static String FORMAT_LONOGRAM_CN = "yyyy年MM月dd日";
    /**
     * 精确到时分秒的完整时间（中文）eg:2019年11月11日 12时12分12秒
     */
	public static String FORMAT_FULL_CN = "yyyy年MM月dd日 HH时mm分ss秒";
	/**
	 * 精确到毫秒完整时间（中文）
	 */
	public static String FORMAT_FULL_MILL_CN = "yyyy年MM月dd日 HH时mm分ss秒SSS毫秒";
	/**
	 *预设默认的时间格式
	 */
	public static String getDefaultFormat(){return FORMAT_FULL;};
	
    /**
     * 预设格式格式化日期
     */
    public static String format(Date date) {
        return format(date, getDefaultFormat());
    }
    
    /**
     *自定义格式格式化日期
     */
    public static String format(Date date,String format){
    	String value = "";
    	if(date!=null){
    		SimpleDateFormat sdf = new SimpleDateFormat(format);
    		value = sdf.format(date);
    	}
    	return value;
    }
    
    /**
     * 获取当前系统前一天日期
     * @return
     */
    public static String getYesterday() {
    		Date date = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DAY_OF_MONTH, -1);
            date = calendar.getTime();
            return format(date, FORMAT_LONOGRAM);
        }
    
    /**
	 *根据预设默认格式，返回当前日期
	 */
	public static String getNow(){return format(new Date());}
	
	/**
	 *自定义时间格式，返回当前日期
	 */
	public static String getNow(String format){return format(new Date(),format);}

	/**
	 *根据预设默认时间 String->Date
	 */
	public static Date parse(String strDate){return parse(strDate,getDefaultFormat());}
	
	/**
	 *自定义时间格式：Stirng->Date
	 */
	public static Date parse(String strDate,String format){
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		try{
			return sdf.parse(strDate);
		}catch(ParseException e){
			//e.printStackTrace();
			log.error("根据预设默认时间 异常:", e);
			return null;
		}
	}

	/**
	 *基于指定日期增加年
	 *@param num  整数往后推，负数往前移
	 */
	public static Date addYearbth(Date date,int num){
    	Calendar cal= Calendar.getInstance();
    	cal.setTime(date);
    	cal.add(Calendar.YEAR,num);
    	return cal.getTime();
    }

	/**
	 *基于指定日期增加整月
	 *@param num  整数往后推，负数往前移
	 */
    public static Date addMobth(Date date,int num){
    	Calendar cal= Calendar.getInstance();
    	cal.setTime(date);
    	cal.add(Calendar.MONTH,num);
    	return cal.getTime();
    }
    
    /**
	 *基于指定日期增加天数
	 *@param num  整数往后推，负数往前移
	 */
	public static Date addDay(Date date, int num) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, num);
        return cal.getTime();
    }

    /**
	 *基于指定日期增加分钟
	 *@param num  整数往后推，负数往前移
	 */
	public static Date addMinute(Date date, int num) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, num);
        return cal.getTime();
    }

	/**
	 *获取时间戳 eg:yyyy-MM-dd HH:mm:ss.S
	 */
	public static String getTimeStamp(){
		SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_FULL_MILL);
		Calendar cal = Calendar.getInstance();
		return sdf.format(cal.getTime());
	}
	
	/**
	 *获取日期年份
	 */
	public static String getYear(Date date){return format(date).substring(0,4);}

	/**
	 *获取年份+月
	 */
	public static String getYearMonth(Date date){return format(date).substring(0,7);}
	
	/**
	 *获取日期的小时数
	 */
	public static int getHour(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.HOUR_OF_DAY);
	}

	/**
	 *自定义时间格式字符串距离今天的天数
	 */
	public static int countDays(String strDate,String format){
		long time = Calendar.getInstance().getTime().getTime();
		Calendar cal = Calendar.getInstance();
		cal.setTime(parse(strDate,format));
		long diff = cal.getTime().getTime();
		return (int) (time/1000 - diff/1000)/3600/24;
	}

	/**
	 * 预设格式的字符串距离今天的天数
	 */
	public static int countDays(String strDate){return countDays(strDate,getDefaultFormat());}

	 /**
     * 获取天数差值(依赖时间)
     * @return
     */
    public static int diffDays(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
        	return 0;
        }
        return (int) (Math.abs(date1.getTime() - date2.getTime()) / (60 * 60 * 24 * 1000));
    }
 
    /**
     *获取年份差值
     */
    public static int diffYear(Date year1,Date year2){return diffDays(year1,year2) / 365;}

	 /**
     * 获取天数差值(依赖Date类型的日期)
     * @return
     */
    public static int diffByDays(Date d1, Date d2) {
        Date s1 = parse(format(d1, FORMAT_LONOGRAM ), FORMAT_LONOGRAM );
        Date s2 = parse(format(d2, FORMAT_LONOGRAM ), FORMAT_LONOGRAM );
        return diffDays(s1, s2);
    }
	
	 /**
     * 获取时间分割集合
     *
     * @param date 查询日期
     * @param strs 带拆分的时间点
     * @return
     */
    public static List<Date> collectTimes(Date date, String[] strs) {
        List<Date> result = new ArrayList<>();
        List<String> times = Arrays.asList(strs);
        String dateStr = format(date, FORMAT_LONOGRAM );
        String pattern = FORMAT_LONOGRAM + " k";
        if (times.size() > 0) {
            times.stream().forEach(t -> {
                result.add(parse(dateStr + " " + t, pattern));
            });
        }
        return result;
    }

	 /**
     * 根据日期查询当前为周几
     * @param dt
     * @return
     */
    public static String getWeekOfDate(Date dt) {
        String[] weekDays = {"1", "2", "3", "4", "5", "6", "7"};
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        int w = cal.get(Calendar.DAY_OF_WEEK);
        if (0 == w) {
            w = 7;
        }
        return weekDays[w];
    }
		
	public static String intToCn(int hourInt, String[] timeArray) {
        String result = "";
        if (0 <= hourInt && hourInt <= 10) {
            result += timeArray[hourInt] + "\n";
        } else if (11 <= hourInt && hourInt <= 19) {
            result += (timeArray[10] + "\n" + timeArray[hourInt % 10]) + "\n";
        } else {
            result += (timeArray[hourInt / 10] + "\n" + timeArray[10] + "\n" + (hourInt % 10 == 0 ? "" : timeArray[hourInt % 10] + "\n"));
        }
        return result;
    }
	
	/**
     * 将时间转换成汉字
     * @param hour
     * @return
     */
    public  static String hourToCn(String hour){
    	String[] timeArray = {"零", "一", "二", "三", "四", "五", "六", "七", "八", "九", "十"};
    	String[] hourArray = hour.split(":");
    	int hourInt = Integer.parseInt(hourArray[0]);
        int minute = Integer.parseInt(hourArray[1]);
        String result = intToCn(hourInt, timeArray) + "点\n" + intToCn(minute, timeArray) + "分";
        return result;
    }

	/**
     * 获取当前日期后的一周时间，并返回LinkedHashMap<String, Date>
     * @param startTime
     * @return
     */
    public static LinkedHashMap<String, Date> dateAfterWeek(String startTime) {
        LinkedHashMap<String, Date> result = new LinkedHashMap<>();
        try {
            Date date = parse(startTime,FORMAT_LONOGRAM);
            for (int i = 0; i < 7; i++) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                calendar.add(calendar.DATE, i); //把日期往后增加一天,整数往后推,负数往前移动  时间戳转时间
                Date newDate = calendar.getTime();
                String str = new SimpleDateFormat("yyyy-MM-dd").format(newDate);
                result.put(str, newDate);
            }
        } catch (Exception e) {
            //e.printStackTrace();
            log.debug("获取当前日期 后的一周时间异常:", e);
        }
        return result;
    }

	/**
     * 获取当前日期 后的一周时间，并返回yyyy-MM-dd字符串数组
     * @param startTime
     * @return
     */
    public static String[] dateAfterWeekArray(String startTime) {
        String[] weekArray = new String[7];
        try {
            Date date = parse(startTime,FORMAT_LONOGRAM);
            for (int i = 0; i < 7; i++) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                calendar.add(calendar.DATE, i); //把日期往后增加一天,整数往后推,负数往前移动  时间戳转时间
                Date newDate = calendar.getTime();
                weekArray[i] = new SimpleDateFormat("yyyy-MM-dd").format(newDate);
            }
        } catch (Exception e) {
            //e.printStackTrace();
            log.debug("获取当前日期 后的一周时间异常:", e);
        }
        return weekArray;
    }
}