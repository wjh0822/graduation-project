package com.study.common.utils;

import java.math.BigDecimal;

/**
 * 常量
 * 
 * @author zhangxiong
 * @version $Id: InitValueConstant.java zhangxiong $
 * @date 2020年5月20日 上午10:26:46
 *
 */
public class InitValueConstant {

	public static final String STR_ZERO = "0";

	public static final String STR_ONE = "1";
	public static final String STR_TWO = "2";
	public static final String STR_THREE = "3";
	public static final String STR_FOUR = "4";
	public static final String STR_FIVE = "5";

	public static final BigDecimal BIG_DECIMAL_ZERO = new BigDecimal(0);
	public static final BigDecimal BIG_DECIMAL_ONE = new BigDecimal(1);
	public static final BigDecimal BIG_DECIMAL_CENT_ONE = new BigDecimal("0.01");
	public static final BigDecimal BIG_DECIMAL_HUNDRED = new BigDecimal(100);
	public static final BigDecimal BIG_DECIMAL_MINUS_ONE = new BigDecimal(-1);

	public static final String CUR_CODE_RMB = "01";

	public static final String MYSQL = "mysql";
	public static final String STARTINDEX = "startIndex";
	public static final String PAGENO = "pageNo";
	public static final String PAGEFRISTITEM = "pageFristItem";
	public static final String PAGELASTITEM = "pageLastItem";
	public static final String DATA = "data";
	public static final String TOTALITEM = "totalItem";
	public static final String CODE = "code";
	public static final String MESSAGE = "message";
	public static final String SUCCESS = "SUCCESS";
	public static final BigDecimal MAX_NOTICE_NUM = new BigDecimal(9999);

}
