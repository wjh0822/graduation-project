package com.study.common.bo;

import com.study.common.query.QueryBase;

public class BaseResponse extends QueryBase {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8733113301094864550L;

	/** 处理结果编码 */
	private String code;
	/** 处理结果描述 */
	private String message;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
