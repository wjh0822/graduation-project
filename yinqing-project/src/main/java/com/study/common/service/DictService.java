package com.study.common.service;

import com.study.common.reqeust.DictRequest;
import com.study.common.response.DictResponse;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 数据字典查询服务
 * </p>
 * 
 * @author zhangx
 * @version $Id: DictService.java 2020年3月16日 上午10:20:37 zhangx $
 */
@Service
public interface DictService {
	
	/**
	 * 分页获取数据字典项
	 * @param request
	 * @return
	 */
	public DictResponse queryDictPage(DictRequest request);

	/**
	 * 根据类型获取数据字典项
	 * @param request
	 * @return
	 */
	public DictResponse queryDictByType(DictRequest request);

	/**
	 * 新增数据字典项
	 * @param request
	 * @return
	 */
	public DictResponse addDict(DictRequest request);
	
	/**
	 * 修改数据字典项
	 * @param request
	 * @return
	 */
	public DictResponse modifyDict(DictRequest request);
	
	/**
	 * 删除数据字典项
	 * @param request
	 * @return
	 */
	public DictResponse removeDict(DictRequest request);

}
