/*
 * Hundsun Inc.
 * Copyright (c) 2006-2016 All Rights Reserved.
 *
 * Author     :zhangx
 * Version    :1.0
 * Create Date:2016-11-10
 *
 */
package com.study.common.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.study.common.dao.IBaseDao;
import com.study.common.domain.IBaseDomain;
import com.study.common.query.QueryPage;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;


/**
 * 分页服务基类
 * 
 * @author zhangx
 * @version $Id: BaseQueryService.java 2016-11-10 上午9:27:35 zhangx $
 */
public abstract class BaseQueryService<V extends QueryPage, T extends IBaseDomain> {

	/**
	 * 获取基础dao
	 * @return
	 */
	protected abstract IBaseDao<T> getBaseDao();

	@Value("com.mysql.jdbc.Driver")
	private String dataBaseType;

	public void paginationList(V page, Map<String, Object> map) {
		if (!(page instanceof QueryPage)) {
			throw new IllegalArgumentException("'page' argument is unsupport class type, it must be " + QueryPage.class.getName() + " or subclass");
		}
		if (null == map) {
			map = new HashMap<String, Object>();
		}
		//将pageNo设置到currentPage
		if(page.getPageNo()!=null){
			page.setCurrentPage(page.getPageNo());
		}
		// 获取对应的域dao
		IBaseDao<T> baseDao = getBaseDao();
		// 查询总记录数
		int totalCount = baseDao.getAllCount(map);
		page.setTotalItem(totalCount);
		if (totalCount > 0) {
			// 设置分页参数 查询分页记录
			String mysqlFlag = "mysql";
			if (StringUtils.contains(dataBaseType, mysqlFlag)) {
				map.put("startIndex", page.getMysqlPageFristItem());
				map.put("pageNo", page.getPageSize());
			} else {
				map.put("pageFristItem", page.getPageFristItem());
				map.put("pageLastItem", page.getPageLastItem());
			}
			List<T> list = baseDao.getAll(map);
			page.setItems(list);
		}

	}

}
