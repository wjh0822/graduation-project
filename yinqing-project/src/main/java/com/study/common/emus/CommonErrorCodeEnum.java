package com.study.common.emus;

import java.util.HashMap;
import java.util.Map;

public enum CommonErrorCodeEnum {
	
	/**
	 * 处理结果
	 */
	SUCCESS("0000", "成功"), 
	FAILED("9998", "处理失败"), 
	ERROR("9999", "服务异常请联系管理员"), 
	PARAMS_NULL("0001", "必填参数为空"), 
	PARAMS_ERROR("0002", "参数错误"), 
	UPDATE_DICT_EMPTY("0003", "字典更新失败"), 
	REMOVE_DICT_EMPTY("0004", "字典删除失败"), 
	ADD_DICT_EMPTY("0005", "字典新增失败"), 
	SYS_DAILY_PROCESS("0006", "系统清算，暂停账务操作"), 
	//调用dw异常
	DW_CALL_ERROR("8000","获取数据异常"),
	//dw返回查询错误
	DW_RETURN_ERROR("8001","获取数据失败"),
	DW_ANALYSIS_ERROR("8002","解析数据失败")

	;

	private String code;
	private String message;

	private CommonErrorCodeEnum(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public static Map<String, String> toMap() {
		Map<String, String> map = new HashMap<String, String>();
		for (CommonErrorCodeEnum item : CommonErrorCodeEnum.values()) {
			map.put(item.getCode(), item.getMessage());
		}
		return map;
	}

}
