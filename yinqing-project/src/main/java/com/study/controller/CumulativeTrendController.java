package com.study.controller;

import com.alibaba.fastjson.JSONObject;
import com.study.common.emus.CommonErrorCodeEnum;
import com.study.request.CumulativeTrendRequest;
import com.study.request.NewTrendRequest;
import com.study.response.CumulativeTrendResponse;
import com.study.response.NewTrendResponse;
import com.study.service.CumulativeTrendService;
import com.study.service.NewTrendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: liyuankun
 * @Date: 2021/2/8 17:18
 * @Description: 累计趋势
 **/
@RestController
@RequestMapping("/api/cumulativeTrend")
public class CumulativeTrendController {

    @Autowired
    private CumulativeTrendService cumulativeTrendService;

    /**
     * 累计趋势查询
     * @param request
     * @return
     */
    @RequestMapping(value = "/select",produces="application/json;charset=utf-8")
    public String queryCumulativeTrend(CumulativeTrendRequest request){
        CumulativeTrendResponse response = null;
        try {
            response = cumulativeTrendService.queryCumulativeTrend(request);
        }catch(Exception e) {
            response = new CumulativeTrendResponse();
            response.setCode(CommonErrorCodeEnum.ERROR.getCode());
            response.setMessage(CommonErrorCodeEnum.ERROR.getMessage());
        }
        return JSONObject.toJSONString(response);
    }

}
