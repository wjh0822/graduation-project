package com.study.controller;
import com.alibaba.fastjson.JSONObject;
import com.study.common.emus.CommonErrorCodeEnum;
import com.study.request.BasicDataAddRequest;
import com.study.response.BasicDataAddResponse;
import com.study.service.BasicDataAddService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: liyuankun
 * @Date: 2021/1/31 16:12
 * @Description: 基础数据
 **/

@RestController
@RequestMapping("/api/basicDataAdd")
public class BasicDataAddController {

    @Autowired
    private BasicDataAddService basicDataAddService;

    /**
     * 基础数据新增最新查询
     * @param request
     * @return
     */
    @RequestMapping(value = "/select",produces="application/json;charset=utf-8")
    public String queryBasicDataAdd(BasicDataAddRequest request){
        BasicDataAddResponse response = null;
        try {
            response = basicDataAddService.queryBasicDataAddLast();
        }catch(Exception e) {
            response = new BasicDataAddResponse();
            response.setCode(CommonErrorCodeEnum.ERROR.getCode());
            response.setMessage(CommonErrorCodeEnum.ERROR.getMessage());
        }
        return JSONObject.toJSONString(response);
    }

}
