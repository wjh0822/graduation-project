package com.study.response;


import com.study.bo.CumulativeTrendBO;
import com.study.common.bo.BaseResponse;

import java.util.ArrayList;

public class CumulativeTrendResponse extends BaseResponse {

    private static final long serialVersionUID = 1L;


    private ArrayList<CumulativeTrendBO> list = new ArrayList<CumulativeTrendBO>();

    public ArrayList<CumulativeTrendBO> getList() {
        return list;
    }

    public void setList(ArrayList<CumulativeTrendBO> list) {
        this.list = list;
    }

}
