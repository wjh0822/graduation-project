package com.study.response;


import com.study.bo.BasicDataBO;
import com.study.common.bo.BaseResponse;

import java.util.ArrayList;

public class BasicDataResponse extends BaseResponse {

    private static final long serialVersionUID = 1L;


    private ArrayList<BasicDataBO> list = new ArrayList<BasicDataBO>();

    public ArrayList<BasicDataBO> getList() {
        return list;
    }

    public void setList(ArrayList<BasicDataBO> list) {
        this.list = list;
    }

}
