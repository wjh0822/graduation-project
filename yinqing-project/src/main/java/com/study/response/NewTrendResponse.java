package com.study.response;


import com.study.bo.NewTrendBO;
import com.study.common.bo.BaseResponse;

import java.util.ArrayList;

public class NewTrendResponse extends BaseResponse {

    private static final long serialVersionUID = 1L;


    private ArrayList<NewTrendBO> list = new ArrayList<NewTrendBO>();

    public ArrayList<NewTrendBO> getList() {
        return list;
    }

    public void setList(ArrayList<NewTrendBO> list) {
        this.list = list;
    }

}
