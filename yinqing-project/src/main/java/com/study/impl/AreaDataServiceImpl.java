package com.study.impl;

import com.alibaba.fastjson.JSON;
import com.study.bo.AreaDataBO;
import com.study.bo.BasicDataBO;
import com.study.common.emus.CommonErrorCodeEnum;
import com.study.common.utils.BeanConvertUtil;
import com.study.common.utils.DateTimeUtil;
import com.study.dao.AreaDataMapper;
import com.study.enums.AreaUrlEnum;
import com.study.enums.WebSocketEnum;
import com.study.pojo.AreaData;
import com.study.pojo.BasicData;
import com.study.request.AreaDataRequest;
import com.study.request.BasicDataRequest;
import com.study.response.AreaDataResponse;
import com.study.response.BasicDataResponse;
import com.study.service.AreaDataService;
import com.study.service.WebSocket;
import com.study.utils.HtmlParseUtils;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: liyuankun
 * @Date: 2021/2/9 16:56
 * @Description:  区域数据
 **/
@Component
public class AreaDataServiceImpl implements AreaDataService {

    private static final Logger logger = LoggerFactory.getLogger(AreaDataServiceImpl.class);

    @Autowired
    private AreaDataMapper areaDataMapper;

    @Autowired
    private WebSocket webSocket;

    @Override
    public AreaDataResponse queryAreaData(AreaDataRequest request) {
        AreaDataResponse response = new AreaDataResponse();
        response.setCode(CommonErrorCodeEnum.ERROR.getCode());
        response.setMessage(CommonErrorCodeEnum.ERROR.getMessage());
        try {
            // 数据信息
            List<AreaData> list = areaDataMapper.getList(BeanConvertUtil.bean2Map(request));
            if (CollectionUtils.isNotEmpty(list)) {
                ArrayList<AreaDataBO> boList = new ArrayList<AreaDataBO>();
                for (AreaData areaData : list) {
                    AreaDataBO bo = BeanConvertUtil.mapToBean(BeanConvertUtil.bean2Map(areaData), new AreaDataBO());
                    boList.add(bo);
                }
                response.setList(boList);
            }
            response.setCode(CommonErrorCodeEnum.SUCCESS.getCode());
            response.setMessage(CommonErrorCodeEnum.SUCCESS.getCode());
        } catch (IllegalArgumentException e) {
            response.setCode(CommonErrorCodeEnum.PARAMS_NULL.getCode());
            response.setMessage(e.getMessage());
        } catch (Exception e) {
            // slf4j 打印异常
            logger.error("<<<<<< queryAreaData error cause", e);
        }
        return response;
    }

    @Override
    public AreaDataResponse getAreaData() {
        AreaDataResponse response = new AreaDataResponse();
        response.setCode(CommonErrorCodeEnum.ERROR.getCode());
        response.setMessage(CommonErrorCodeEnum.ERROR.getMessage());
        try {
            AreaDataRequest areaDataRequest = new AreaDataRequest();
            areaDataRequest.setDate(DateTimeUtil.getNow("yyyy-MM-dd"));
            AreaDataResponse areaDataResponse = queryAreaData(areaDataRequest);
            Map<String, Object> map = new HashMap<>();
            List<AreaData> areaList = new ArrayList<>();
            AreaUrlEnum[] values = AreaUrlEnum.values();
            if (CollectionUtils.isEmpty(areaDataResponse.getList())) {
                for (int i = 0; i < 31; i++) {
                    AreaData areaData = HtmlParseUtils.jsonToMapAreaData(values[i].getUrl());
                    areaList.add(areaData);
                }
                List<AreaData> areaDataList = HtmlParseUtils.jsonToMapAreaData();
                for (AreaData areaData : areaDataList) {
                    areaList.add(areaData);
                }
            } else {
                response.setCode(CommonErrorCodeEnum.SUCCESS.getCode());
                response.setMessage(CommonErrorCodeEnum.SUCCESS.getMessage());
                return response;
            }
            map.put("areaList", areaList);
            int i = areaDataMapper.insertByForeach(map);
            if (i > 0) {
                //发送websocket消息
//                BasicDataResponse lastResponse = queryBasicDataLast();
//                lastResponse.setCode(WebSocketEnum.GetBasicData.getCode());
//                webSocket.sendMessage(JSON.toJSONString(lastResponse));
                response.setCode(CommonErrorCodeEnum.SUCCESS.getCode());
                response.setMessage(CommonErrorCodeEnum.SUCCESS.getMessage());
            }
        } catch (Exception e) {
            logger.error("getAreaData  error cause", e);
        }
        return response;
    }
}
