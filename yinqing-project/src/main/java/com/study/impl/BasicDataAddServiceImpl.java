package com.study.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.study.bo.BasicDataAddBO;
import com.study.bo.BasicDataBO;
import com.study.common.emus.CommonErrorCodeEnum;
import com.study.common.utils.BeanConvertUtil;
import com.study.dao.BasicDataAddMapper;
import com.study.enums.WebSocketEnum;
import com.study.pojo.BasicDataAdd;
import com.study.request.BasicDataAddRequest;
import com.study.request.BasicDataRequest;
import com.study.response.BasicDataAddResponse;
import com.study.response.BasicDataResponse;
import com.study.service.BasicDataAddService;
import com.study.service.WebSocket;
import com.study.utils.HtmlParseUtils;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: liyuankun
 * @Date: 2021/1/28 15:21
 * @Description:
 **/
@Component
public class BasicDataAddServiceImpl implements BasicDataAddService {

    private static final Logger logger = LoggerFactory.getLogger(BasicDataAddServiceImpl.class);

    @Autowired
    private BasicDataAddMapper basicDataAddMapper;

    @Autowired
    private WebSocket webSocket;

    @Override
    public BasicDataAddResponse queryBasicAddData(BasicDataAddRequest request) {
        BasicDataAddResponse response = new BasicDataAddResponse();
        response.setCode(CommonErrorCodeEnum.ERROR.getCode());
        response.setMessage(CommonErrorCodeEnum.ERROR.getMessage());
        try {
            // 数据信息
            List<BasicDataAdd> list = basicDataAddMapper.getList(BeanConvertUtil.bean2Map(request));
            if (CollectionUtils.isNotEmpty(list)) {
                ArrayList<BasicDataAddBO> boList = new ArrayList<BasicDataAddBO>();
                for (BasicDataAdd basicDataAdd : list) {
                    BasicDataAddBO bo = BeanConvertUtil.mapToBean(BeanConvertUtil.bean2Map(basicDataAdd), new BasicDataAddBO());
                    boList.add(bo);
                }
                response.setList(boList);
            }
            response.setCode(CommonErrorCodeEnum.SUCCESS.getCode());
            response.setMessage(CommonErrorCodeEnum.SUCCESS.getCode());
        } catch (IllegalArgumentException e) {
            response.setCode(CommonErrorCodeEnum.PARAMS_NULL.getCode());
            response.setMessage(e.getMessage());
        } catch (Exception e) {
            // slf4j 打印异常
            logger.error("<<<<<< queryBasicAddData error cause", e);
        }
        return response;
    }

    @Override
    public BasicDataAddResponse getBasicDataAdd() {
        BasicDataAddResponse response = new BasicDataAddResponse();
        response.setCode(CommonErrorCodeEnum.ERROR.getCode());
        response.setMessage(CommonErrorCodeEnum.ERROR.getMessage());
        try {
            BasicDataAddResponse basicDataAddResponse = queryBasicAddData(new BasicDataAddRequest());
            List<String> descList = new ArrayList<>();
            for (BasicDataAddBO basicDataAddBO : basicDataAddResponse.getList()) {
                descList.add(basicDataAddBO.getLocalTime());
            }
            List<String> list = HtmlParseUtils.jsonToMapBasicData();
            List<BasicDataAdd> basicDataAddList = new ArrayList<>();
            Map<String, Object> map = new HashMap<>();
            if (!descList.contains(list.get(0))) {
                BasicDataAdd basicDataAdd = new BasicDataAdd();
                basicDataAdd.setLocalTime(list.get(0));
                basicDataAdd.setExistingConfirmedAdd(list.get(9));
                basicDataAdd.setAsymptomaticAdd(list.get(10));
                basicDataAdd.setExistingSuspectAdd(list.get(11));
                basicDataAdd.setExistingSevereIllnessAdd(list.get(12));
                basicDataAdd.setCumulativeDiagnosisAdd(list.get(13));
                basicDataAdd.setImportAbroadAdd(list.get(14));
                basicDataAdd.setCumulativeCureAdd(list.get(15));
                basicDataAdd.setCumulativeDeathsAdd(list.get(16));
                basicDataAddList.add(basicDataAdd);
            }else {
                response.setCode(CommonErrorCodeEnum.SUCCESS.getCode());
                response.setMessage(CommonErrorCodeEnum.SUCCESS.getMessage());
                return response;
            }
            map.put("basicDataAddList", basicDataAddList);
            int i = basicDataAddMapper.insertByForeach(map);
            if (i > 0) {
                //发送websocket消息
                BasicDataAddResponse lastResponse = queryBasicDataAddLast();
                lastResponse.setCode(WebSocketEnum.GetBasicDataAdd.getCode());
                webSocket.sendMessage(JSON.toJSONString(lastResponse));
                response.setCode(CommonErrorCodeEnum.SUCCESS.getCode());
                response.setMessage(CommonErrorCodeEnum.SUCCESS.getMessage());
            }
        } catch (Exception e) {
            logger.error("getBasicDataAdd  error cause", e);
        }
        return response;
    }

    @Override
    public BasicDataAddResponse queryBasicDataAddLast() {
        BasicDataAddResponse response = new BasicDataAddResponse();
        response.setCode(CommonErrorCodeEnum.ERROR.getCode());
        response.setMessage(CommonErrorCodeEnum.ERROR.getMessage());
        try {
            BasicDataAddResponse basicDataAddResponse = queryBasicAddData(new BasicDataAddRequest());
            BasicDataAddBO basicDataAddBO = basicDataAddResponse.getList().get(basicDataAddResponse.getList().size() - 1);
            ArrayList<BasicDataAddBO> list = new ArrayList<>();
            list.add(basicDataAddBO);
            response.setList(list);
            response.setCode(CommonErrorCodeEnum.SUCCESS.getCode());
            response.setMessage(CommonErrorCodeEnum.SUCCESS.getMessage());
        } catch (Exception e) {
            logger.error("queryBasicDataAddLast  error cause", e);
        }
        return response;
    }
}
