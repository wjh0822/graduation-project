package com.study.impl;

import com.alibaba.fastjson.JSON;
import com.study.bo.BasicDataBO;
import com.study.common.emus.CommonErrorCodeEnum;
import com.study.common.utils.BeanConvertUtil;
import com.study.dao.BasicDataMapper;
import com.study.enums.WebSocketEnum;
import com.study.pojo.BasicData;
import com.study.request.BasicDataRequest;
import com.study.response.BasicDataResponse;
import com.study.service.BasicDataService;
import com.study.service.WebSocket;
import com.study.utils.HtmlParseUtils;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: liyuankun
 * @Date: 2021/1/28 15:21
 * @Description: 基础数据
 **/
@Component
public class BasicDataServiceImpl implements BasicDataService {

    private static final Logger logger = LoggerFactory.getLogger(BasicDataServiceImpl.class);

    @Autowired
    private BasicDataMapper basicDataMapper;

    @Autowired
    private WebSocket webSocket;

    @Override
    public BasicDataResponse queryBasicData(BasicDataRequest request) {
        BasicDataResponse response = new BasicDataResponse();
        response.setCode(CommonErrorCodeEnum.ERROR.getCode());
        response.setMessage(CommonErrorCodeEnum.ERROR.getMessage());
        try {
            // 数据信息
            List<BasicData> list = basicDataMapper.getList(BeanConvertUtil.bean2Map(request));
            if (CollectionUtils.isNotEmpty(list)) {
                ArrayList<BasicDataBO> boList = new ArrayList<BasicDataBO>();
                for (BasicData basicData : list) {
                    BasicDataBO bo = BeanConvertUtil.mapToBean(BeanConvertUtil.bean2Map(basicData), new BasicDataBO());
                    boList.add(bo);
                }
                response.setList(boList);
            }
            response.setCode(CommonErrorCodeEnum.SUCCESS.getCode());
            response.setMessage(CommonErrorCodeEnum.SUCCESS.getCode());
        } catch (IllegalArgumentException e) {
            response.setCode(CommonErrorCodeEnum.PARAMS_NULL.getCode());
            response.setMessage(e.getMessage());
        } catch (Exception e) {
            // slf4j 打印异常
            logger.error("<<<<<< queryBasicData error cause", e);
        }
        return response;
    }

    @Override
    public BasicDataResponse getBasicData() {
        BasicDataResponse response = new BasicDataResponse();
        response.setCode(CommonErrorCodeEnum.ERROR.getCode());
        response.setMessage(CommonErrorCodeEnum.ERROR.getMessage());
        try {
            BasicDataResponse basicDataResponse = queryBasicData(new BasicDataRequest());
            List<String> descList = new ArrayList<>();
            for (BasicDataBO basicDataBO : basicDataResponse.getList()) {
                descList.add(basicDataBO.getLocalTime());
            }
            List<String> list = HtmlParseUtils.jsonToMapBasicData();
            List<BasicData> basicDataList = new ArrayList<>();
            Map<String, Object> map = new HashMap<>();
            if (!descList.contains(list.get(0))) {
                BasicData basicData = new BasicData();
                basicData.setLocalTime(list.get(0));
                basicData.setExistingConfirmed(list.get(1));
                basicData.setAsymptomatic(list.get(2));
                basicData.setExistingSuspect(list.get(3));
                basicData.setExistingSevereIllness(list.get(4));
                basicData.setCumulativeDiagnosis(list.get(5));
                basicData.setImportAbroad(list.get(6));
                basicData.setCumulativeCure(list.get(7));
                basicData.setCumulativeDeaths(list.get(8));
                basicDataList.add(basicData);
            }else {
                response.setCode(CommonErrorCodeEnum.SUCCESS.getCode());
                response.setMessage(CommonErrorCodeEnum.SUCCESS.getMessage());
                return response;
            }
            map.put("basicDataList", basicDataList);
            int i = basicDataMapper.insertByForeach(map);
            if (i > 0) {
                //发送websocket消息
                BasicDataResponse lastResponse = queryBasicDataLast();
                lastResponse.setCode(WebSocketEnum.GetBasicData.getCode());
                webSocket.sendMessage(JSON.toJSONString(lastResponse));
                response.setCode(CommonErrorCodeEnum.SUCCESS.getCode());
                response.setMessage(CommonErrorCodeEnum.SUCCESS.getMessage());
            }
        } catch (Exception e) {
            logger.error("getBasicData  error cause", e);
        }
        return response;
    }

    @Override
    public BasicDataResponse queryBasicDataLast() {
        BasicDataResponse response = new BasicDataResponse();
        response.setCode(CommonErrorCodeEnum.ERROR.getCode());
        response.setMessage(CommonErrorCodeEnum.ERROR.getMessage());
        try {
            BasicDataResponse basicDataResponse = queryBasicData(new BasicDataRequest());
            BasicDataBO basicDataBO = basicDataResponse.getList().get(basicDataResponse.getList().size() - 1);
            ArrayList<BasicDataBO> list = new ArrayList<>();
            list.add(basicDataBO);
            response.setList(list);
            response.setCode(CommonErrorCodeEnum.SUCCESS.getCode());
            response.setMessage(CommonErrorCodeEnum.SUCCESS.getMessage());
        } catch (Exception e) {
            logger.error("queryBasicDataLast  error cause", e);
        }
        return response;
    }
}
